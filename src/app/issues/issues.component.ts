import { Component, OnInit } from '@angular/core';
import {IssuesService} from "./issues.service";

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.scss']
})
export class IssuesComponent implements OnInit {

  constructor(private _issuesService: IssuesService) { }
  issues;
  lastUpdated;
  loading: boolean;
  selectedIndex;
  issueTabs = ['Design', 'Content'];
  selectedTab = 'Design';
  ngOnInit() {
    this.loading = true;
    this.selectedIndex = 0;
    this.getIssues();
  }

  getIssues () {
    this._issuesService.getIssues().subscribe(
      (data) => {
        console.log(data)
        this.issues = data['data'] || []
        this.lastUpdated = this.issues[0].issueTime;
        this.loading = false;
      }
    )
  }

  setActiveCell(index) {
    this.selectedIndex = index;
  }

  changeTab (tab) {
    this.selectedTab = tab;
  }
}

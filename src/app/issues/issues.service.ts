import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class IssuesService {
  baseURL = 'http://localhost:4000/';
  constructor(private http: HttpClient) { }

  getIssues () {
    let url = this.baseURL + 'formatedIssues';
    return this.http.get(url);
  }
}

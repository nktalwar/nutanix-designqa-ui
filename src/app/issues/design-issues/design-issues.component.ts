import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-design-issues',
  templateUrl: './design-issues.component.html',
  styleUrls: ['./design-issues.component.scss']
})
export class DesignIssuesComponent implements OnInit {

  @Input() issues = [];
  type;
  issuesLength;
  issuesMapper = [
    {
      key: 'className',
      display: 'Class Name'
    },
    {
      key: 'issueType',
      display: 'Issue Type'
    },
    {
      key: 'issueValue',
      display: 'Issue Value'
    },
    {
      key: 'type',
      display: 'CSS Property'
    },
  ];
  expanded: boolean = false;
  constructor() { }

  ngOnInit() {
    console.log(this.issues);
    this.type = this.issues[0].issueType.replace(/([a-z0-9])([A-Z])/g, '$1 $2')
    this.issuesLength = this.issues.length;
  }

  expandRow() {
    this.expanded = !this.expanded;
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignIssuesComponent } from './design-issues.component';

describe('DesignIssuesComponent', () => {
  let component: DesignIssuesComponent;
  let fixture: ComponentFixture<DesignIssuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignIssuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignIssuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

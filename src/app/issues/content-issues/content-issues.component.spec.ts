import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentIssuesComponent } from './content-issues.component';

describe('ContentIssuesComponent', () => {
  let component: ContentIssuesComponent;
  let fixture: ComponentFixture<ContentIssuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentIssuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentIssuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-content-issues',
  templateUrl: './content-issues.component.html',
  styleUrls: ['./content-issues.component.scss']
})
export class ContentIssuesComponent implements OnInit {

  @Input() textIssues;
  constructor() { }

  ngOnInit() {
  }

}

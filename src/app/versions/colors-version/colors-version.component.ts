import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-colors-version',
  templateUrl: './colors-version.component.html',
  styleUrls: ['./colors-version.component.scss']
})
export class ColorsVersionComponent implements OnInit {

  @Input() colors = [];
  displayColors;
  type;
  expanded: boolean = false;
  constructor() { }

  ngOnInit() {
    console.log(this.colors);
    this.getHexCode();
  }

  expandRow() {
    this.expanded = !this.expanded;
  }
  getHexCode() {
    this.displayColors = [];
    for(let i = 0; i < this.colors.length; i++) {
      let obj= {
        rgb: this.colors[i],
        hex: this.RGBToHex(this.colors[i])
      }
      this.displayColors.push(obj);
    }
  }

  RGBToHex(rgb) {
    // Choose correct separator
    let sep = rgb.indexOf(",") > -1 ? "," : " ";
    // Turn "rgb(r,g,b)" into [r,g,b]
    rgb = rgb.substr(4).split(")")[0].split(sep);

    let r = (+rgb[0]).toString(16),
      g = (+rgb[1]).toString(16),
      b = (+rgb[2]).toString(16);

    if (r.length == 1)
      r = "0" + r;
    if (g.length == 1)
      g = "0" + g;
    if (b.length == 1)
      b = "0" + b;

    return "#" + r + g + b;
  }

}

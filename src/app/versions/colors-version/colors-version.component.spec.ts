import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorsVersionComponent } from './colors-version.component';

describe('ColorsVersionComponent', () => {
  let component: ColorsVersionComponent;
  let fixture: ComponentFixture<ColorsVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColorsVersionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorsVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

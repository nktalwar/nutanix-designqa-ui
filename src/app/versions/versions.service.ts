import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VersionsService {

  baseURL = 'http://localhost:4000/';
  constructor(private http: HttpClient) { }

  getVersions () {
    let url = this.baseURL + 'versions';
    return this.http.get(url);
  }
}

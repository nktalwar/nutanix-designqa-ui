import { Component, OnInit } from '@angular/core';

import {VersionsService} from './versions.service';

@Component({
  selector: 'app-versions',
  templateUrl: './versions.component.html',
  styleUrls: ['./versions.component.scss']
})
export class VersionsComponent implements OnInit {

  constructor(private _versionService: VersionsService) { }
  verisons;
  lastUpdated;
  loading: boolean;
  selectedIndex;
  issueTabs = ['Design', 'Content'];
  selectedTab = 'Design';
  ngOnInit() {
    this.loading = true;
    this.selectedIndex = 0;
    this.getIssues();
  }

  getIssues () {
    this._versionService.getVersions().subscribe(
      (data) => {
        console.log(data)
        this.verisons = data['data'] || []
        this.lastUpdated = this.verisons[0].issueTime;
        this.loading = false;
      }
    )
  }

  setActiveCell(index) {
    this.selectedIndex = index;
  }

  changeTab (tab) {
    this.selectedTab = tab;
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {VersionsComponent} from './versions/versions.component';
import {IssuesComponent} from './issues/issues.component';


const routes: Routes = [{
  path: 'version',
  component: VersionsComponent
},
  {
    path: 'issues',
    component: IssuesComponent
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { IssuesComponent } from './issues/issues.component';
import {HttpClientModule} from "@angular/common/http";
import {MomentModule} from "angular2-moment";
import { DesignIssuesComponent } from './issues/design-issues/design-issues.component';
import { ContentIssuesComponent } from './issues/content-issues/content-issues.component';
import { VersionsComponent } from './versions/versions.component';
import { ColorsVersionComponent } from './versions/colors-version/colors-version.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    IssuesComponent,
    DesignIssuesComponent,
    ContentIssuesComponent,
    VersionsComponent,
    ColorsVersionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MomentModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
